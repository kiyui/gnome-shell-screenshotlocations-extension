# gnome-screenshot-locations-extension

Override GNOME's screenshot functionality to use a user-defined path, in the order of:

1. User defined path (added behaviour)
2. Pictures directory (default behaviour)
3. Home directory (default behaviour)

![extension preferences](./images/preferences.png)

## installation

Available from GNOME extensions website [here](https://extensions.gnome.org/extension/1179/screenshot-locations/).

## tips

If you enjoy this extension, consider buying me a coffee 😄

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/D1D34NIY5)